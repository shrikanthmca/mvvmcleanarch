package com.sri.data.repository

import com.sri.data.api.RemoteNewsApi
import com.sri.data.mapper.NewsDataEntityMapper
import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.repositories.NewsDataStore

class NewsRemoteImpl constructor(private val api: RemoteNewsApi) : NewsDataStore {

    override suspend fun getNews(): NewsSourcesEntity {
        return newsMapper.mapToEntity(api.getNews())
    }

    private val newsMapper = NewsDataEntityMapper()

}