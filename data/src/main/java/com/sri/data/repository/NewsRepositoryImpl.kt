package com.sri.data.repository

import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.repositories.NewsRepository


class NewsRepositoryImpl(private val remote: NewsRemoteImpl) : NewsRepository {

    override suspend fun getNews(): NewsSourcesEntity {
        return remote.getNews()
    }

}