package com.sri.data.mapper

import com.google.gson.annotations.SerializedName
import com.sri.domain.entities.NewsPublisherEntity
import com.sri.domain.entities.NewsSourcesEntity

data class NewsSourcesData(
    @SerializedName("status") var status: String? = null,
    @SerializedName("articles") var articles: List<NewsPublisherEntity> = emptyList()
)


class NewsDataEntityMapper() {

    fun mapToEntity(data: NewsSourcesData?): NewsSourcesEntity = NewsSourcesEntity(
        status = data?.status,
        articles = mapListArticlesToEntity(data?.articles)
    )

    fun mapListArticlesToEntity(articles: List<NewsPublisherEntity>?)
            : List<NewsPublisherEntity> = articles?.map { mapArticleToEntity(it) } ?: emptyList()

    fun mapArticleToEntity(response: NewsPublisherEntity): NewsPublisherEntity =
        NewsPublisherEntity(
            description = response.description,
            title = response.title,
            author = response.author
        )

}
