package com.sri.data.api

import com.sri.data.mapper.NewsSourcesData
import retrofit2.http.GET

interface RemoteNewsApi {

    @GET("top-headlines?country=us")
    suspend fun getNews(): NewsSourcesData

}