package com.sri.data.repository

import com.sri.data.newsSourcesEntityFailure
import com.sri.data.newsSourcesEntitySuccess
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class NewsRepositoryImplTest {

    private lateinit var newsRepositoryImpl: NewsRepositoryImpl
    @Mock
    private lateinit var newsRemoteImpl: NewsRemoteImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsRepositoryImpl = NewsRepositoryImpl(newsRemoteImpl)
    }

    @Test
    fun shouldTestNewsRepositorySuccess() {
        //given
        Mockito.`when`(newsRemoteImpl.getNews())
            .thenReturn(Flowable.just(newsSourcesEntitySuccess))

        //when
        val test = newsRepositoryImpl.getNews().test()

        //then
        test.assertValue { l -> l.status.equals("success") }
    }

    @Test
    fun shouldTestNewsRepositoryFailure() {

        //given
        Mockito.`when`(newsRepositoryImpl.getNews())
            .thenReturn(Flowable.just(newsSourcesEntityFailure))

        //when
        val test = newsRepositoryImpl.getNews().test()

        //then
        test.assertValue { l -> l.status.equals("failure") }

    }
}