package com.sri.data.repository

import com.sri.data.api.RemoteNewsApi
import com.sri.data.newSourceDataFailure
import com.sri.data.newSourceDataSuccess
import com.sri.domain.entities.NewsPublisherEntity
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class NewsRemoteImplTest {

    @Mock
    private lateinit var api: RemoteNewsApi
    private lateinit var newsRemoteImpl: NewsRemoteImpl
    private val throwable = Throwable()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsRemoteImpl = NewsRemoteImpl(api)
    }


    @Test
    fun shouldTestGetNewsSuccess() {

        val newDataSource = newSourceDataSuccess

        val article1 =
            NewsPublisherEntity("Srikanth", "title", "desc", "urlimage")
        val article2 =
            NewsPublisherEntity("Shrivin", "title", "desc", "urlimage")
        val listofArticle: MutableList<NewsPublisherEntity> = mutableListOf<NewsPublisherEntity>()
        listofArticle.add(article1)
        listofArticle.add(article2)

        newSourceDataSuccess.articles = listofArticle

        //given
        `when`(api.getNews()).thenReturn(Flowable.just(newDataSource))

        //when
        val test = newsRemoteImpl.getNews().test()

        //then
        val news = verify(api).getNews()
        test.assertValue { l -> l.articles.size == 2 }
        test.assertValue { l -> l.status.equals("success") }
    }

    @Test
    fun shouldTestGetNewsFailure() {

        val newDataSource = newSourceDataFailure
        //given
        `when`(api.getNews()).thenReturn(Flowable.just(newDataSource))

        //when
        val test = newsRemoteImpl.getNews().test()

        //then
        test.assertValue { l -> l.status.equals("failure") }
    }

    @Test
    fun shouldTestGetNewsRemoteFail() {

        //given
        `when`(api.getNews()).thenReturn(Flowable.error(throwable))

        //when
        val test = newsRemoteImpl.getNews().test()

        //then
        verify(api).getNews()
        test.assertError(throwable)
    }
}