package com.sri.data

import com.sri.data.mapper.NewsSourcesData
import com.sri.domain.entities.NewsSourcesEntity

val newSourceDataSuccess = NewsSourcesData("success")
val newSourceDataFailure = NewsSourcesData("failure")

val newsSourcesEntitySuccess = NewsSourcesEntity("success")
val newsSourcesEntityFailure = NewsSourcesEntity("failure")

val newsSourcesData = NewsSourcesData("success")