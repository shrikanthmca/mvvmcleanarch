package com.sri.data.model

import com.sri.data.newsSourcesData
import org.junit.Test

class NewsSourcesDataTest {

    @Test
    fun shouldTestNewsSourceData() {

        //then
        assert(newsSourcesData.status == "success")
        assert(newsSourcesData.articles.isEmpty())

    }

}