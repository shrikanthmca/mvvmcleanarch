package com.sri.mvvmcleanarch.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.usecase.GetNewsUsecase
import com.sri.mvvmcleanarch.presentation.entities.Data
import com.sri.mvvmcleanarch.presentation.entities.NewsSources
import com.sri.mvvmcleanarch.presentation.entities.Status
import com.sri.mvvmcleanarch.presentation.mappers.Mapper
import io.reactivex.Flowable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class NewsViewModelTest {

    private lateinit var newsViewModel: NewsViewModel
    @Mock
    private lateinit var getNewsUsecase: GetNewsUsecase
    @Mock
    private lateinit var mapper: Mapper<NewsSourcesEntity, NewsSources>

    private val throwable = Throwable()

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        newsViewModel = NewsViewModel(getNewsUsecase, mapper)
    }

    @Test
    fun shouldTestGetNewsSuccess() {

        //given
        val newsSources = NewsSources("success")
        Mockito.`when`(mapper.Flowable(newsSourcesEntitySuccess))
            .thenReturn(Flowable.just(newsSources))
        Mockito.`when`(getNewsUsecase.getNews()).thenReturn(Flowable.just(newsSourcesEntitySuccess))

        //when
        newsViewModel.getNews()

        //then
        Assert.assertEquals(
            Data(responseType = Status.SUCCESSFUL, data = newsSources, error = null),
            newsViewModel.getNewsLiveData().value
        )
    }

    @Test
    fun shouldTestGetNewsFailure() {
        //given
        Mockito.`when`(getNewsUsecase.getNews()).thenReturn(Flowable.error(throwable))

        //when
        newsViewModel.getNews()

        //then
        Assert.assertEquals(
            Data(
                responseType = Status.ERROR,
                data = null,
                error = Error(throwable.message)
            ).toString().trim(),
            newsViewModel.news.value.toString().trim()
        )
    }
}