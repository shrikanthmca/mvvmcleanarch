package com.sri.mvvmcleanarch.presentation.view.news

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sri.mvvmcleanarch.R
import com.sri.mvvmcleanarch.presentation.viewmodel.NewsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val newsViewModel: NewsViewModel by inject()
    private lateinit var adapter: NewsRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = NewsRecyclerAdapter()
        recycler_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_view.adapter = adapter

        // Observe newslist and update our adapter if we get new one from API
        newsViewModel.getNewsLiveData().observe(this, Observer {
            it?.articles?.let { response -> adapter.updateNewsList(response) }
        })

        // Observe showLoading value and display or hide our activity's progressBar
        newsViewModel.showLoading.observe(this, Observer { showLoading ->
            mainProgressBar.visibility = if (showLoading!!) View.VISIBLE else View.GONE
        })

        // Observe showError value and display the error message as a Toast
        newsViewModel.showError.observe(this, Observer { showError ->
            Toast.makeText(this, showError, Toast.LENGTH_SHORT).show()
        })
    }

}
