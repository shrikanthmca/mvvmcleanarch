package com.sri.mvvmcleanarch.presentation.di

import com.sri.data.api.RemoteNewsApi
import com.sri.data.repository.NewsRemoteImpl
import com.sri.data.repository.NewsRepositoryImpl
import com.sri.domain.repositories.NewsRepository
import com.sri.domain.usecase.GetNewsUsecase
import com.sri.mvvmcleanarch.BuildConfig
import com.sri.mvvmcleanarch.presentation.networking.createNetworkClient
import com.sri.mvvmcleanarch.presentation.viewmodel.NewsViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit


private const val RETROFIT_INSTANCE = "Retrofit"
private const val API = "Api"
private const val GET_NEWS_USECASE = "getNewsUseCase"


val mViewModels = module {
    viewModel {
        NewsViewModel(getNewsUsecase = get(GET_NEWS_USECASE))
    }
}

val mNetworkModules = module {
    single(name = RETROFIT_INSTANCE) { createNetworkClient(BuildConfig.BASE_URL) }
    single(name = API) { (get(RETROFIT_INSTANCE) as Retrofit).create(RemoteNewsApi::class.java) }
}

val mRepositoryModules = module {
    single(name = "remote") { NewsRemoteImpl(api = get(API)) }
    single { NewsRepositoryImpl(remote = get("remote")) as NewsRepository }
}


val mUseCaseModules = module {
    factory(name = "getNewsUseCase") {
        GetNewsUsecase(
            newsRepository = get()
        )
    }
}
