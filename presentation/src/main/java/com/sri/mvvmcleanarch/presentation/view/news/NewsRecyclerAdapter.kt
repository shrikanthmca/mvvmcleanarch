package com.sri.mvvmcleanarch.presentation.view.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sri.domain.entities.NewsPublisherEntity
import com.sri.mvvmcleanarch.R
import com.sri.mvvmcleanarch.presentation.entities.NewsPublisher
import kotlinx.android.synthetic.main.news_row.view.*

class NewsRecyclerAdapter : RecyclerView.Adapter<NewsRecyclerAdapter.NewsViewHolder>() {

    var listOfArticles = mutableListOf<NewsPublisherEntity>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_row, parent, false)
        return NewsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listOfArticles.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(listOfArticles.get(position))
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(newsPublisherItem: NewsPublisherEntity) {
            with(itemView) {
                tv_title.text = newsPublisherItem.title
                tv_author.text = newsPublisherItem.author
                tv_description.text = newsPublisherItem.description
            }
        }
    }

    fun updateNewsList(list: List<NewsPublisherEntity>) {
        if (list.isNotEmpty()) {
            listOfArticles.clear()
            listOfArticles.addAll(list)
            notifyDataSetChanged()

        }
    }

}