package com.sri.mvvmcleanarch.presentation

import android.app.Application
import com.sri.mvvmcleanarch.presentation.di.mNetworkModules
import com.sri.mvvmcleanarch.presentation.di.mRepositoryModules
import com.sri.mvvmcleanarch.presentation.di.mUseCaseModules
import com.sri.mvvmcleanarch.presentation.di.mViewModels
import org.koin.android.ext.android.startKoin

class NewsApp : Application() {

    override fun onCreate() {
        super.onCreate()
        loadKoinDi()
    }

    private fun loadKoinDi() {
        startKoin(
            this, listOf(
                mNetworkModules,
                mRepositoryModules,
                mViewModels,
                mUseCaseModules
            )
        )
    }

}