package com.sri.mvvmcleanarch.presentation.entities

data class NewsPublisher(
    var title: String? = null,
    var author: String? = null,
    var description: String? = null
)