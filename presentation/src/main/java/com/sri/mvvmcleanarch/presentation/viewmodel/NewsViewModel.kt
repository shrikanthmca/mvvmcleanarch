package com.sri.mvvmcleanarch.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import com.sri.domain.common.ServiceResult
import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.usecase.GetNewsUsecase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NewsViewModel(private val getNewsUsecase: GetNewsUsecase) : BaseViewModel() {

    var news = MutableLiveData<NewsSourcesEntity>()
    val showError = MutableLiveData<String>()
    val showLoading = MutableLiveData<Boolean>()

    init {
        loadData()
    }

    private fun loadData() {

        // Show progressBar during the operation on the MAIN (default) thread
        showLoading.value = true

        // launch the Coroutine
        launch {

            // Switching from MAIN to IO thread for API operation
            // Update our data list with the new one from API
            val result = withContext(Dispatchers.IO) {
                getNewsUsecase.getCoNews()
            }

            // Hide progressBar once the operation is done on the MAIN (default) thread
            showLoading.value = false

            when (result) {
                is ServiceResult.Success -> news.value = result.data
                is ServiceResult.Error -> showError.value = result.exception.message
            }

        }

    }

    fun getNewsLiveData() = news

}