package com.sri.domain.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.whenever
import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.repositories.NewsRepository
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class GetNewsUsecaseTest {

    private lateinit var usecase: GetNewsUsecase
    @Mock
    private lateinit var mockRepository: NewsRepository


    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {

        MockitoAnnotations.initMocks(this)

        val transformer =
            Mockito.mock(FlowableRxTransformer::class.java)

        usecase =
            GetNewsUsecase(transformer as FlowableRxTransformer<NewsSourcesEntity>, mockRepository)
    }

    @Test
    fun shouldTestGetnewsFromDomainLayer() {
        //given
        whenever(mockRepository.getNews())
            .thenReturn(Flowable.just(NewsSourcesEntity("success")))

        //when
        val test = usecase.getNews().test()

        //then
        test.assertValue { l -> l.equals("success") }
    }
}