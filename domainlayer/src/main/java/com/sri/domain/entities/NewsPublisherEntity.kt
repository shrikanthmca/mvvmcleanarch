package com.sri.domain.entities

data class NewsPublisherEntity(
    var author: String? = null,
    var title: String? = null,
    var description: String? = null,
    var urlToImage: String? = null
)