package com.sri.domain.usecase

import com.sri.domain.common.ServiceResult
import com.sri.domain.entities.NewsSourcesEntity
import com.sri.domain.repositories.NewsRepository

class GetNewsUsecase(private val newsRepository: NewsRepository) {

    suspend fun getCoNews(): ServiceResult<NewsSourcesEntity> {

        return try {
            val result = newsRepository.getNews()
            ServiceResult.Success(result)
        } catch (ex: Exception) {
            ServiceResult.Error(ex)
        }

    }

}