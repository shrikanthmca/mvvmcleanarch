package com.sri.domain.common

sealed class ServiceResult<out T : Any> {
    class Success<out T : Any>(val data: T) : ServiceResult<T>()
    class Error(val exception: Throwable) : ServiceResult<Nothing>()
    class Loading<out T : Any> : ServiceResult<T>()
    class NoInternetResult<T : Any> : ServiceResult<T>()
}