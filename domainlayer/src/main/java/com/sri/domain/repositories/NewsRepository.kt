package com.sri.domain.repositories

import com.sri.domain.entities.NewsSourcesEntity

interface NewsRepository {
    suspend fun getNews(): NewsSourcesEntity
}