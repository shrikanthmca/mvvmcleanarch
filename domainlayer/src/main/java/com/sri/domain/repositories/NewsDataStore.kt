package com.sri.domain.repositories

import com.sri.domain.entities.NewsSourcesEntity

interface NewsDataStore {

    suspend fun getNews(): NewsSourcesEntity

}